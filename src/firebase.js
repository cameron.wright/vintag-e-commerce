import firebase from 'firebase';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBzNvPvJcmGaiD5IH4PtppFQM_3Y0LSJJc",
    authDomain: "vintag-e-commerce.firebaseapp.com",
    databaseURL: "https://vintag-e-commerce.firebaseio.com",
    projectId: "vintag-e-commerce",
    storageBucket: "vintag-e-commerce.appspot.com",
    messagingSenderId: "447270264178",
    appId: "1:447270264178:web:151f270aeb30051db751eb",
    measurementId: "G-KXSLWWJ91G"
  };

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();
const auth =  firebase.auth();

export { db, auth };