import React from 'react'
import './Header.css';
import headerLogo from './images/w_logo_1.png';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { Link } from 'react-router-dom';
import { useStateValue } from './StateProvider';
import { auth } from './firebase'

function Header() {
    // eslint-disable-next-line no-unused-vars
    const [{basket, user}, dispatch] = useStateValue();

    const handleAuthentication = () =>{
        if (user){
            auth.signOut()
        }
    }
    const navSlide = ()=>{
        const headerNav = document.querySelector('.header__nav')
        const headerNavOptions = document.querySelectorAll('.header__nav div');

        headerNavOptions.forEach((option, index)=>{
            if(option.style.animation){
                option.style.animation = '';
            }else{
                option.style.animation = `navLinkFade 0.5s ease forwards ${index/7}s`

            }
        })

        headerNav.classList.toggle('header_navActive')
    }

    return (
        <div className='header'>
            <Link to='/'>
                <img className='header__logo' src={headerLogo} alt='logo'/>
            </Link>
            <div className='header__search'>
                <input className='header__searchInput' type='text'/>
                <SearchIcon className='header__searchIcon' />
            </div>
            <div className='header__nav'>
                <Link to={!user && '/login'}>
                    <div onClick={handleAuthentication} className='header__option'>
                        <span className='header__optionLineOne'>Hello {user ? user.email.split('@')[0].split('.')[0] : 'Guest'}</span>
                        <span className='header__optionLinetwo'>{user ? 'Sign Out' : 'Sign In'}</span>
                    </div>
                </Link>
                
                <div className='header__option'>
                <span className='header__optionLineOne'>Product</span>
                    <span className='header__optionLinetwo'>Categories</span>
                </div>
            </div>
            <Link to='/checkout'>
                <div className='header__optionBasket'>
                    <ShoppingBasketIcon />
                    <span className='header__optionLineTwo header__basketCount'>{basket?.length}</span>
                </div>
            </Link>
            <div className='header__burger' onClick={navSlide}>
                <div className='line1'></div>
                <div className='line1'></div>
                <div className='line1'></div>
            </div>
            
        </div>
        
    )
}

export default Header;
