import React from 'react'
import './Payment.css';
import { useStateValue } from './StateProvider';
import CheckoutProduct from './CheckoutProduct.js';
import PayPal from './PayPal.js';
import { Link } from 'react-router-dom';

function Payment() {
    // eslint-disable-next-line no-unused-vars
    const [{ basket, user }, dispatch] = useStateValue();

    return (
        <div className='payment'>
            <h1>
                Checkout {<Link to='/checkout'>{basket?.length} items</Link>}
            </h1>
            <div className='payment__section'>
                <div className='payment__title'>
                    <h3>Delivery Address</h3>
                </div>
                <div className='payment__address'>
                    <p>{user?.email}</p>
                    <p>9 Rivers Edge Estate</p>
                    <p>93 Wild Avenue</p>
                    <p>Newlands, Pretoria</p>
                    <p>0049</p>
                </div>
            </div>

            <div className='payment__section'>
                <div className='payment__title'>
                    <h3>Review items and delivery</h3>
                </div>
                <div className='payment__items'>
                    {basket.map(item=>(
                    <CheckoutProduct
                    id={item.id}
                    image={item.image}
                    title={item.title}
                    price={item.price}
                    rating={item.rating}  />
                    ))}
                </div>
            </div>

            <div className='payment__section'>
                <div className='payment__title'>
                    <h3>Payment Method</h3>
                </div>
                <div className='payment__details'>
                    <PayPal />
                </div>
            </div>
        </div>
    )
}

export default Payment
