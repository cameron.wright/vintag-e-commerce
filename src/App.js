import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import './App.css';
import Header from './Header.js';
import Checkout from './Checkout.js';
import OrderSuccessful from './OrderConfirmation.js';
import ProductDetails from './ProductDetails.js';
import Login from './Login.js';
import Home from './Home.js';
import Payment from './Payment.js';
import { auth } from './firebase';
import { useStateValue } from './StateProvider';

function App() {
    // eslint-disable-next-line no-empty-pattern
    const [{}, dispatch] = useStateValue()

    useEffect(() => {
      //will only run once when app component loads
      auth.onAuthStateChanged(authUser=>{
        if(authUser){
          dispatch({
            type: 'SET_USER',
            user: authUser
          })
        }else{
          dispatch({
            type: 'SET_USER',
            user: null
          })
        }
      })
    }, [dispatch])

  return (
    <Router>
      <div className="App">
        <Switch>
        <Route path='/login'>
            <Login />
          </Route>
          <Route path='/checkout'>
            <Header />
            <Checkout />
          </Route>
          <Route path='/payment'>
            <Header />
            <Payment />
          </Route>
          <Route path='/orderConfirmation'>
            <Header />
            <OrderSuccessful />
          </Route>
          <Route path='/productDetails/:id'>
            <Header />
            <ProductDetails />
          </Route>
          <Route path='/'>
            <Header />
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;