import React from 'react'
import './ProductDetails.css';
import { useStateValue } from './StateProvider';
import { useParams, Link } from 'react-router-dom'


function ProductDetails() {
    // eslint-disable-next-line no-empty-pattern
    const [{}, dispatch] = useStateValue();

    const { id } = useParams();

    return (
        <div className='productDetails'>
            <div className='productDetails__container'>
                <div className='productDetails__titleContainer'>
                    <p className='productDetails__title'>
                        Title
                    </p>
                    <Link to='/'>
                        <button>Back</button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default ProductDetails
