import React, { useState } from 'react'
import ReactDOM from "react-dom"
import { useStateValue } from './StateProvider';
import { Redirect } from "react-router-dom";

const PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });

function PayPal() {

  const [{ basket }, dispatch] = useStateValue();

  const [retrievedOrderDetails, setRetrievedOrderDetails] = useState()
  
  const createOrder = (data, actions) => {

    let subTotal = 0;

    const items = [];

    basket.forEach(item => {
      subTotal = subTotal + item.price

      items.push({
        name: `${item.title}`,
        unit_amount: {
          value: `${item.price}`,
          currency_code: 'USD',
        },
        quantity: "1",
        sku: `${item.id}`
      })
    })

    subTotal = subTotal.toFixed(2);

    return actions.order.create({
      purchase_units: [{
        description: 'vintag-e-commerce_ORDER',
        amount: {
          value: `${subTotal}`,
          currency_code: 'USD',
          breakdown: {
            item_total: {
              value: `${subTotal}`,
              currency_code: 'USD',
            },
            shipping: {
              value: '0',
              currency_code: 'USD',
            },
            handling: {
              value: '0',
              currency_code: 'USD',
            },
            tax_total: {
              value: '0',
              currency_code: 'USD',
            },
            insurance: {
              value: '0',
              currency_code: 'USD',
            },
          },
        },
        items,
      },],
    });
  };

  const onApprove = (data, actions) => {
    
    return actions.order.capture()
    .then(() => actions.order.get())
      .then(paymentDetails =>{
        console.log("Payment Deets: ", paymentDetails);

        setPaymentDetails(paymentDetails);
        setRetrievedOrderDetails(true);
        
    });
  };

      
  const setPaymentDetails = (paymentDetails) =>{
    dispatch({
      type: 'SET_PAYMENT_DETAILS',
      paymentDetails
    })
  };

  return (

    retrievedOrderDetails ? 
      <Redirect to="/orderConfirmation" /> : 
      <PayPalButton
        createOrder={(data, actions) => createOrder(data, actions)}
        onApprove={(data, actions) => onApprove(data, actions)}
        onClick={console.log("Button Clicked")}
      />
  );
}

export default PayPal
