import React from 'react'
import Subtotal from './Subtotal.js';
import './Checkout.css';
import { useStateValue } from './StateProvider.js';
import CheckoutProduct from './CheckoutProduct.js';


function Checkout() {
    // eslint-disable-next-line no-unused-vars
    const [{ basket, user }, dispatch] = useStateValue();
    return (
        <div className='checkout'>
            <div className='checkout__left'>
                <h3>Hello, {user?.email}</h3>
                <h2 className='checkout__title'>Your Shopping Basket</h2>
                {basket.map(item=>(
                    <CheckoutProduct
                    id={item.id}
                    image={item.image}
                    title={item.title}
                    price={item.price}
                    rating={item.rating}  />
                ))}
            </div>
            <div className='checkout__right'>
                <Subtotal />
            </div>
        </div>
    )
}

export default Checkout
