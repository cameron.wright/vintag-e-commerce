import React from 'react'
import './OrderConfirmation.css'
import paymentComplete from './images/circled_checkmark.svg';
import { Link } from 'react-router-dom';
import { useStateValue } from './StateProvider';

function OrderSuccessful() {

    // eslint-disable-next-line no-unused-vars
    const [{ paymentDetails }, dispatch] = useStateValue();
    
    return (
        <div className='orderSuccessful'>
            <h1>Payment successful</h1>
            <img className="orderSuccessful__paymentCompleteIcon" src={paymentComplete} alt="paymentCompleteIcon" />
            <h2>We've got your order <span role="img" aria-label="Winking-Face">😉</span> Order number: {paymentDetails.id}</h2>
            <p>Thank you for your support. We celebrate with every purchase made by our customers.</p>
            <br />
            <p>Check your email for your order confirmation and shipping details. If you don't see an order confirmation email in your mailbox please email <a href="mailto:cameron.n.wright@gmail.com">cameron.n.wright@gmail.com</a> and we'll get back to you in 24 hours.</p>
            <br />
            <p>Shipping details are discussed in the order confirmation email but you can refer to our{" "}<a href="/shipping">shipping</a>{" "}page for more information.</p>
            <Link to="/" className="btn btn-primary">Continue Shopping</Link>
        </div>
    )
}

export default OrderSuccessful
