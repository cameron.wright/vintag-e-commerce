import React from 'react'
import './Home.css';
import homeSplash from './images/splash_image.jpg';
import Product from './Product';
import productImage1 from './images/products/VRS-2800-ESP_Alt_1_700x700.jpg';
import productImage2 from './images/products/VC-525_1_700x700.jpg';
import productImage3 from './images/products/VTA-270B-ESP_Main_700x700.jpg';
import productImage4 from './images/products/VS-140-WNT_1_700x700.jpg';
import productImage5 from './images/products/VS-130-BLK-ALT_1_700x700.jpg';
import productImage6 from './images/products/ITSBO-513PS10Main_700x700.jpg';


function Home() {
    return (
        <div className='home'>
            <img className='home__splash' src={homeSplash} alt="homeSplashImage"/>
            <div className='home__row'>
                <Product 
                id={111}
                title='Victrola Retro Wood Bluetooth FM/AM Radio with Rotary Dial'
                price={1.7}
                image={productImage1}
                rating={5}/>
                <Product 
                id={666}
                title='The Empire'
                price={1.4}
                image={productImage3}
                rating={5}/>
            </div>
            <div className='home__row'>
                <Product 
                id={222}
                title='Victrola Retro Wood Bluetooth FM/AM Radio with Rotary Dial'
                price={1.6}
                image={productImage2}
                rating={5}/>
                <Product
                id={333}
                title='Victrola Bluetooth Wood Speaker Stand with Dual USB Ports'
                price={60}
                image={productImage4}
                rating={5} />
                <Product
                id={444}
                title='Victrola Bluetooth Wood Speaker Stand with Dual USB Ports'
                price={1.2}
                image={productImage6}
                rating={5} />
            </div>
            <div className='home__row'>
                <Product 
                id={555}
                title='Victrola 30 Watt Bluetooth® Hi-Fi Speaker with Glossy Piano Finish'
                price={1.3}
                image={productImage5}
                rating={5}/>
            </div>
        </div>
    )
}

export default Home
