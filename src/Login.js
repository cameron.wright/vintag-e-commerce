import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom';
import './Login.css';
import logo from './images/w_logo_1.png';
import { auth } from './firebase';

function Login() {
    const history = useHistory()
    const [email, setEmail] = useState()
    const [password, setPasword] = useState()

    const signIn = e => {
        e.preventDefault()
        
        auth
            .signInWithEmailAndPassword(email, password)
            .then(auth=>{
                history.push('/')
            })
            .catch(error=> alert(error.message))

        
    }

    const register = e =>{
        e.preventDefault()

        auth
            .createUserWithEmailAndPassword(email, password)
            .then((auth) =>{
                console.log(auth)
                if(auth){
                    history.push('./');
                }
            })
            .catch(error=>alert(error.message))
    }

    return (
        <div className='login'>
            <Link to='/'>
                <img className='login__logo' src={logo} alt="logo"/>
            </Link>
            <div className='login__container'>
                <h1>Sign in</h1>
                <form>
                    <h5>Email</h5>
                    <input type='text' value={email} onChange={e => setEmail(e.target.value)}/>

                    <h5>Password</h5>
                    <input type='password' value={password} onChange={e=>setPasword(e.target.value)}/>
                    <button type='submit' className='login__signInButton' onClick={signIn}>Sign in</button>
                </form>
                <p>
                    By signing in you agree to  Wrighter's Conditions of Use.
                </p>
                <button className='login__registerButton' onClick={register}>Create an Account</button>
            </div>
        </div>
    )
}

export default Login
